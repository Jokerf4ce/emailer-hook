function Decoder (str) {
	return str.replace(/&#([0-9]{1,3});/gi, function(match, numStr) {
		var num = parseInt(numStr, 10); // read num as normal number
		return String.fromCharCode(num);
	}); 
}

function Emailer_hook(hash,t_site,t_item) {     
	var url = window.location.href.split('/')
	url.pop();
	var site = url[url.length-1];
	if ( site == t_site ) {
		var text = Decoder(String(hash));
		var element = document.getElementById(t_item);
		element.setAttribute('href',text);
		var arr = text.split(':')
		element.innerHTML = arr[1];         
	}
}


// http://www.katpatuka.org/pub/doc/anti-spam.html > to get html entities



	